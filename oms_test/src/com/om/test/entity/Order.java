package com.om.test.entity;

public class Order {
	
	private String orderName;
	private String orderDescription;
	private int quantity;
	private String orderType;
	
	public Order(){
	}
	
	public Order(String orderName, String orderDescription, int quantity, String orderType) {
		this.orderName = orderName;
		this.orderDescription = orderDescription;
		this.quantity = quantity;
		this.orderType = orderType;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
}
